
![ClarityMoe](nodebot_logo.png)

[![Official Discord server](https://discordapp.com/api/guilds/251664386459041792/embed.png)](https://discord.gg/rmMTZue)
[![GitHub license](https://img.shields.io/badge/license-BSD-blue.svg)](https://raw.githubusercontent.com/awau/Clara/master/LICENSE)
[![Code Climate](https://codeclimate.com/github/ClarityMoe/Clara/badges/gpa.svg)](https://codeclimate.com/github/ClarityMoe/Clara)
[![Koding](https://koding-cdn.s3.amazonaws.com/badges/made-with-koding/v1/koding_badge_ReadmeDark.png)](https://koding.com)
[![CircleCI](https://circleci.com/gh/ClarityMoe/Clara.svg?style=svg)](https://circleci.com/gh/ClarityMoe/Clara)

[Support us on Patreon!](https://www.patreon.com/capuccino) | [Discord Server](https://discord.gg/ZgQkCkm)


# Running

Go to the [Getting Started](https://github.com/ClarityMoe/Clara/wiki/Getting-Started) page in the wiki for full instructions.

# Contributing

Check out CONTRIBUTING.md for guidelines.

# License
Clara is licensed under the 3-Clause BSD License.  
Copyright &copy; 2017 ClarityMoe.

See [LICENSE](LICENSE) for full license.